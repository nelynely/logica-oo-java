package com.nelynely.zoologico.app;

import com.nelynely.zoologico.classes.Animal;
import com.nelynely.zoologico.classes.Cachorro;
import com.nelynely.zoologico.classes.Gato;
import com.nelynely.zoologico.classes.Veterinario;
import com.nelynely.zoologico.classes.Zoologico;

public class Programa {

	public static void main(String[] args) {
		Animal animal = new Cachorro("Tot�");
		animal.setEspecie("Cachorro");
		animal.setIdade(1);
		System.out.println("Ol�, o seu animal � um " + animal.getEspecie() + ", o nome do "
		+ "animal � " + animal.getNome() + " e ele tem " + animal.getIdade() + " anos.");
		if (animal.ehAdulto()) {
			System.out.println("Cachorro adulto: ");
		} else {
			System.out.println("Cachorro n�o � adulto: ");
		}
		
		
		System.out.println("========================");
		System.out.println("Barulho do cachorro: ");
		animal.emitirBarulho();
		System.out.println("*************************");
		Zoologico zoo = new Zoologico();
		zoo.setNome("Zoo Treinaweb");
		zoo.adicionarAnimal(animal);
		Animal animal2 = new Gato("Eniac", 2);
		zoo.adicionarAnimal(animal2);
		System.out.println("Animais do zool�gico " + zoo.getNome());
		zoo.listarAnimais();
		zoo.removerAnimal(0);
		System.out.println("Depois da remo��o: ");
		zoo.listarAnimais();
		
		Veterinario vet = new Veterinario();
		vet.setNome("Treinaweb");
		vet.atenderAnimal(animal);
		vet.listarAnimaisAtendidos();
	}

}
